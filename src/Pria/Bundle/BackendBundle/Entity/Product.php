<?php

namespace Pria\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Pria\Bundle\BackendBundle\Constraints as ProductAssert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface; 

/**
 * Product
 *
 * @ORM\Table()
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"isFileUploaded"})
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="ext_link", type="text")
     * @Assert\NotBlank
     * @ProductAssert\CheckUrl
     */
    private $extLink;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Assert\Image(
     *       maxSize = "2M",
     *      maxWidth = 2000,
     *      maxHeight = 2000
     *  )
     */
    private $file;

    private $tempName;

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getExtLink()
    {
        return $this->extLink;
    }

    /**
     * @param string $extLink
     */
    public function setExtLink($extLink)
    {
        $this->extLink = $extLink;
    }

    /**
     * @return string
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        if (isset($this->image)) {
            $this->tempName = $this->image;
            $this->image = null;
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if ($this->getFile() !== null) {
            $imageName = md5(uniqid()).'.'.$this->getFile()->guessExtension();
            $this->setImage($imageName);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        $this->getFile()->move(__DIR__.'/../../../../../web/uploads/', $this->getImage());

        if (isset($this->tempName)) {
            unlink(__DIR__.'/../../../../../web/uploads/'.$this->tempName);
            $this->tempName = null;
        }

        $this->setFile(null);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $image = $this->getImage();
        if ($image) {
            unlink(__DIR__.'/../../../../../web/uploads/'.$image);
        }
    }

    public function isFileUploaded(ExecutionContextInterface $context)
    {
        if (null === $this->image && null === $this->file) {
            $context->addViolationAt('file', 'You have to choose a file', array(), null);
        }
    }

}

