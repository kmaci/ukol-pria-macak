<?php

namespace Pria\Bundle\BackendBundle\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class CheckUrl extends Constraint
{
    public $message = 'URL "%s%" is not valid. URL should start with http:// or https://. Please check your URL.';
}