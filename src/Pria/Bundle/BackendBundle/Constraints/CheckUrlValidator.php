<?php

namespace Pria\Bundle\BackendBundle\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckUrlValidator extends ConstraintValidator
{
    public function validate($link, Constraint $constraint)
    {
        if (filter_var($link, FILTER_VALIDATE_URL) === FALSE) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%s%' , $link)
                ->addViolation();
        }
    }
}