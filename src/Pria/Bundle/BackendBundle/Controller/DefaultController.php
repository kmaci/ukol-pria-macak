<?php

namespace Pria\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Pria\Bundle\BackendBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pria\Bundle\BackendBundle\Form\ProductType;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="pria_backend_homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pr = $em->getRepository('PriaBackendBundle:Product')->findAll();

        $currentPage = $request->query->get('page', 1);

        $adapter = new ArrayAdapter($pr);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);

        $pagerfanta->setCurrentPage($currentPage);

        return $this->render('PriaBackendBundle:Default:index.html.twig', 
            ['products' => $pagerfanta]
        );
    }

    /**
     * @Route("/new", name="pria_backend_new")
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(new ProductType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirect($this->generateUrl('pria_backend_homepage'));
        }

        return $this->render('PriaBackendBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/remove/{id}", name="pria_backend_delete")
     */
    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('PriaBackendBundle:Product')->find($id);
        $em->remove($product);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'notice',
            'Product was deleted.'
        );

        return $this->redirect($this->generateUrl('pria_backend_homepage'));
    }

    /**
     * @Route("/update/{id}", name="pria_backend_update")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('PriaBackendBundle:Product')->find($id);

        $form = $this->createForm(new ProductType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Product was updated.'
            );
        }

        return $this->render('PriaBackendBundle:Default:update.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
