<?php
namespace Pria\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('description', 'text')
            ->add('extLink', 'text')
            ->add('file')
            ->add('save', 'submit', array('label' => 'Submit'))
            ->getForm();
    }

    public function getName()
    {
        return 'productForm';
    }
}