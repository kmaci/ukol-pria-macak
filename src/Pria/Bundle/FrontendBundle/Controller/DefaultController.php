<?php

namespace Pria\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="pria_frontend_homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('PriaBackendBundle:Product')->findAll();

       return $this->render('PriaFrontendBundle:Default:index.html.twig', 
            ['products' => $products]
        );
    }

    /**
     * @Route("/update", name="pria_frontend_update")
     */
    public function updateProductInfoAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $id = $this->getRequest()->get('id');
            $product = $em->getRepository('PriaBackendBundle:Product')->find($id);
            return new JsonResponse([
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'extLink' => $product->getExtLink()
            ]);
        }
    }
}
